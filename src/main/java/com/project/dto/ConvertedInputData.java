package com.project.dto;

import com.project.entities.Supplier;
import com.project.entities.Order;
import com.project.entities.Product;
import com.project.entities.Purchaser;
import com.project.entities.PurchaseDate;

import lombok.Data;

@Data
public class ConvertedInputData {
	
	private Supplier supplier;
	
	private Purchaser purchaser;
	
	private Product product;
	
	private PurchaseDate purchaseDate;
	
	private Order order;

}
