package com.project.entitiesrepository;

import org.springframework.data.repository.CrudRepository;

import com.project.entities.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {

}
